 $(function() {
     $("[data-toggle='tooltip']").tooltip();
     $("[data-toggle='popover']").popover();
     $('.corousel').carousel({
         interval: 1000
     });

     $('#contactoBtn').on('show.bs.modal', function(e) {
         console.log('el modal se esta mostrando');
         $('#contactoBtn').removeClass('btn-outline-success');
         $('#contactoBtn').addClass('btn-primary');
         $('#contactoBtn').prop('disabled', true);
     });
     $('#contactoBtn').on('shown.bs.modal', function(e) {
         console.log('el modal se mostró');
     });
     $('#contactoBtn').on('hide.bs.modal', function(e) {
         console.log('el modal se oculta');
     });
     $('#contactoBtn').on('hiden.bs.modal', function(e) {
         console.log('el modal se ocultó');
         $('#contactoBtn').prop('disabled', false);
     });
 })